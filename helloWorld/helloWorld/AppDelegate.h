//
//  AppDelegate.h
//  helloWorld
//
//  Created by Annika Wiesinger on 1/24/17.
//  Copyright © 2017 Annika Wiesinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

