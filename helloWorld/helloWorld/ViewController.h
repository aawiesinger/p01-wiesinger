//
//  ViewController.h
//  helloWorld
//
//  Created by Annika Wiesinger on 1/24/17.
//  Copyright © 2017 Annika Wiesinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *helloLabel;
@property (strong, nonatomic) IBOutlet UIButton *fireButton;
@property (strong, nonatomic) IBOutlet UIButton *secondButton;
@property (strong, nonatomic) IBOutlet UIButton *thirdButton;

@end

