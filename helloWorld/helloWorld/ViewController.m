//
//  ViewController.m
//  helloWorld
//
//  Created by Annika Wiesinger on 1/24/17.
//  Copyright © 2017 Annika Wiesinger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.helloLabel.text = @"Hello 🌍 ";
    
    [self.fireButton setTitle:@"Do not press" forState:UIControlStateNormal];
    
    self.secondButton.hidden=YES;
    self.thirdButton.hidden=YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)handleButtonClick:(id)sender {
        self.secondButton.hidden=NO;
}

- (IBAction)secondButton:(id)sender {
    self.thirdButton.hidden=NO;
}

- (IBAction)thirdButton:(id)sender {
    self.helloLabel.text = @"Annika Wiesinger";
}


@end
