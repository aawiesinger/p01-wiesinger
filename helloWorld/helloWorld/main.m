//
//  main.m
//  helloWorld
//
//  Created by Annika Wiesinger on 1/24/17.
//  Copyright © 2017 Annika Wiesinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
